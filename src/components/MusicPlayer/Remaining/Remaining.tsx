import React, { useContext, useState, useEffect, useRef, useCallback } from 'react';
import { MusicContext } from '../MusicProvider/MusicContext';
import { MusicStatus } from '../MusicProvider/enum';
import { secondsToMinutes } from '../../../helpers';

export const Remaining = () => {
  const ref = useRef<HTMLDivElement>(null);

  const [timer, setTimer] = useState<number>();

  const { getPosition, status, currentMusic, totalLength } = useContext(MusicContext);

  const start = useCallback(() => {
    const timer = window.setInterval(() => {
      const remaining = totalLength - getPosition();
      ref.current.innerText = `-${secondsToMinutes(remaining)}`;
    }, 1000);
    setTimer(timer);
  }, [getPosition, totalLength]);

  const stop = useCallback(() => {
    clearInterval(timer);
  }, [timer]);

  const clean = useCallback(() => {
    stop();
    ref.current.innerText = '';
  }, [stop]);

  // Clean whe music changes
  useEffect(() => {
    clean();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentMusic]);

  // React to music status changes
  useEffect(() => {
    if (currentMusic && status !== MusicStatus.PLAYING) {
      stop();
      // When music is selected and is not playing show at least total length
      if (!ref.current.innerText && totalLength) ref.current.innerText = `-${secondsToMinutes(totalLength)}`;
    } else if (currentMusic && status === MusicStatus.PLAYING) {
      stop();
      start();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentMusic, status, totalLength]);

  return <div className="remaining__box" ref={ref} />;
};
