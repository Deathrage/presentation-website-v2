import { MusicProviderAction, MusicProviderState } from './types';
import { Reducer } from 'react';
import { MusicProviderActions, MusicStatus } from './enum';

const knownHowls: Record<string, Howl> = {};

const getOrCreateHowl = (src: string, format = 'mp3', autoplay = true) => {
  if (!src) return undefined;

  let howl = knownHowls[src];
  if (!howl) {
    howl = new Howl({
      src,
      preload: true,
      format,
    });
    knownHowls[src] = howl;
  }
  if (autoplay) howl.play();

  return howl;
};

const getHowlState = (howl: Howl) => {
  const howlState = howl.state();
  if (howlState === 'unloaded') return MusicStatus.EMPTY;
  else if (howlState === 'loading') return MusicStatus.LOADING;
  else if (howl.playing()) return MusicStatus.PLAYING;
  return MusicStatus.PAUSED;
};

export const musicProviderReducer: Reducer<MusicProviderState, MusicProviderAction> = (
  { howl, src, volume, status },
  { action, payload },
) => {
  switch (action) {
    case MusicProviderActions.SET_MUSIC: {
      howl && howl.stop();
      const newHowl = getOrCreateHowl(payload.src, payload.format, payload.autoplay);
      return {
        howl: newHowl,
        volume,
        src: payload.src,
        status: getHowlState(newHowl),
      };
    }

    case MusicProviderActions.SET_STATUS:
      return { howl, src, status: payload.status ?? MusicStatus.EMPTY, volume };
    case MusicProviderActions.SET_VOLUME:
      return { howl, src, status, volume: payload.volume };
    default:
      return { howl: null, src: null, status: MusicStatus.EMPTY, volume };
  }
};
