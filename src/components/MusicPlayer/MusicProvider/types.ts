import { MusicProviderActions, MusicStatus } from './enum';

export interface MusicProviderState {
  src: string;
  howl: Howl;
  volume: number;
  status: MusicStatus;
}

export interface MusicProviderActionPayload {
  src?: string;
  format?: string;
  autoplay?: boolean;
  status?: MusicStatus;
  volume?: number;
}

export interface MusicProviderAction {
  action: MusicProviderActions;
  payload?: MusicProviderActionPayload;
}

export interface MusicContextType {
  context: AudioContext;
  masterGain: GainNode;
  currentMusic: string;
  totalLength: number;
  status: MusicStatus;
  volume: number;
  switchMusic: (src: string, format?: string, autoplay?: boolean) => void;
  playMusic: () => void;
  pauseMusic: () => void;
  setVolume: (volume: number) => void;
  getPosition: () => number;
}
