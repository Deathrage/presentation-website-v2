export enum MusicProviderActions {
  SET_MUSIC,
  SET_STATUS,
  SET_VOLUME,
}

export enum MusicStatus {
  LOADING,
  PLAYING,
  PAUSED,
  EMPTY,
}
