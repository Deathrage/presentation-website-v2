import React, { FC, useMemo, useReducer, useEffect, useCallback } from 'react';
import { Howler } from 'howler';
import { Alert } from 'rsuite';
import { MusicContext } from './MusicContext';
import { MusicContextType } from './types';
import { musicProviderReducer } from './helpers';
import { MusicProviderActions, MusicStatus } from './enum';
import { constants } from '../../../helpers';

export const MusicProvider: FC<{}> = ({ children }) => {
  const [{ howl, src, status, volume }, dispatch] = useReducer(musicProviderReducer, {
    howl: null,
    src: null,
    volume: Howler.volume(),
    status: MusicStatus.EMPTY,
  });

  useEffect(() => void Howler.volume(100), []);
  useEffect(() => {
    if (howl) {
      howl.on('play', () =>
        dispatch({ action: MusicProviderActions.SET_STATUS, payload: { status: MusicStatus.PLAYING } }),
      );
      howl.on('pause', () =>
        dispatch({ action: MusicProviderActions.SET_STATUS, payload: { status: MusicStatus.PAUSED } }),
      );
      howl.on('end', () =>
        dispatch({ action: MusicProviderActions.SET_STATUS, payload: { status: MusicStatus.PAUSED } }),
      );
      howl.on('load', () =>
        dispatch({ action: MusicProviderActions.SET_STATUS, payload: { status: MusicStatus.PAUSED } }),
      );
      howl.on('loaderror', () => Alert.error(`Could not load the track.`, constants.errorAlertTimeout));
      howl.on('playerror', () => Alert.error(`Could not play the track`, constants.errorAlertTimeout));
      return () => {
        howl.off('play');
        howl.off('pause');
        howl.off('loaderror');
        howl.off('playerror');
        howl.off('end');
        howl.off('load');
      };
    }
  }, [howl]);

  // Event handlers
  const handlePosition = useCallback(() => howl && (howl.seek() as number), [howl]);
  const handleVolume = useCallback((volume: number) => {
    Howler.volume(volume);
    dispatch({ action: MusicProviderActions.SET_VOLUME, payload: { volume } });
  }, []);
  const handlePlay = useCallback(() => howl && howl.play(), [howl]);
  const handlePause = useCallback(() => howl && howl.pause(), [howl]);
  const handleSwitch = useCallback(
    (src: string, format: string, autoplay: boolean) =>
      dispatch({
        action: MusicProviderActions.SET_MUSIC,
        payload: {
          src: src,
          format,
          autoplay,
        },
      }),
    [],
  );

  // Context
  const context = useMemo<MusicContextType>(
    () => ({
      context: Howler.ctx,
      masterGain: Howler.masterGain,
      status,
      currentMusic: src,
      totalLength: howl && howl.duration(),
      volume,
      setVolume: handleVolume,
      playMusic: handlePlay,
      pauseMusic: handlePause,
      switchMusic: handleSwitch,
      getPosition: handlePosition,
    }),
    [handlePause, handlePlay, handlePosition, handleSwitch, handleVolume, howl, src, status, volume],
  );

  return <MusicContext.Provider value={context}>{children}</MusicContext.Provider>;
};
