import { createContext } from 'react';
import { MusicContextType } from './types';
import { MusicStatus } from './enum';

const error = () => {
  throw new Error('Illegal use outside context');
};

export const MusicContext = createContext<MusicContextType>({
  context: null,
  masterGain: null,
  currentMusic: null,
  totalLength: null,
  volume: 100,
  status: MusicStatus.EMPTY,
  switchMusic: error,
  pauseMusic: error,
  playMusic: error,
  setVolume: error,
  getPosition: error,
});
