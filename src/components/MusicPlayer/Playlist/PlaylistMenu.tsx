import React, { FC, useContext, useCallback, useReducer, useEffect, useRef } from 'react';
import { Sidenav, Nav, IconButton, Icon } from 'rsuite';
import { PlaylistMenuProps, PlaylistItem } from './types';
import { PlaylistMenuItem } from './PlaylistMenuItem';
import { MusicContext } from '../MusicProvider/MusicContext';
import { MusicStatus } from '../MusicProvider/enum';
import { AddItem } from './AddItem';
import { playlistReducer } from './helpers';
import { PlaylistActions } from './enum';
import AnimateHeight from 'react-animate-height';
import { useOutboundClick } from '../../../helpers';

export const PlaylistMenu: FC<PlaylistMenuProps> = ({
  initialPlaylist,
  show,
  selectInitialTrack,
  onHide,
  autoplay,
}) => {
  const divRef = useRef<HTMLDivElement>();
  useOutboundClick(divRef, onHide);

  const [playlist, dispatch] = useReducer(playlistReducer, initialPlaylist);
  const { currentMusic, status, switchMusic, playMusic, pauseMusic } = useContext(MusicContext);

  useEffect(() => {
    if (selectInitialTrack) {
      if (!initialPlaylist) throw new Error('Cannot specify prop selectInitialTrack without props initialPlaylist');
      const initialTrack = selectInitialTrack(initialPlaylist);
      initialTrack && switchMusic(initialTrack.src, initialTrack.format, autoplay);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleActiveClick = useCallback(() => {
    switch (status) {
      case MusicStatus.PLAYING:
        return pauseMusic();
      case MusicStatus.PAUSED:
        return playMusic();
      default:
        return;
    }
  }, [pauseMusic, playMusic, status]);

  const handleAddItem = useCallback(
    (item: PlaylistItem) => dispatch({ action: PlaylistActions.ADD_RANGE, payload: [item] }),
    [],
  );

  return (
    <div ref={divRef} className="playlist-menu">
      <AnimateHeight duration={500} height={show ? 'auto' : 0} className="playlist-menu__box">
        <Sidenav appearance="inverse">
          <Sidenav.Header className="playlist-menu__header">
            <IconButton
              className="playlist-menu__close"
              icon={<Icon icon="close" />}
              onClick={onHide}
              appearance="subtle"
            />
            <AddItem onItemProcessed={handleAddItem} />
          </Sidenav.Header>
          <Sidenav.Body>
            <Nav>
              {playlist.map(({ src, format, ...rest }) => {
                const active = src && currentMusic === src;
                return (
                  <PlaylistMenuItem
                    key={src}
                    active={active}
                    status={status}
                    onClick={() => {
                      if (!active) return switchMusic(src, format, autoplay);
                      return handleActiveClick();
                    }}
                    {...rest}
                  />
                );
              })}
            </Nav>
          </Sidenav.Body>
        </Sidenav>
      </AnimateHeight>
    </div>
  );
};
