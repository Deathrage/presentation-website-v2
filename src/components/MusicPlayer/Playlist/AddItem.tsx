import React, { FC, useCallback, useState } from 'react';
import { Uploader, Icon, Loader } from 'rsuite';
import { FileType } from 'rsuite/lib/Uploader';
import { AddItemProps } from './types';
import { processFile } from './helpers';

export const AddItem: FC<AddItemProps> = ({ onItemProcessed }) => {
  const [processing, setProcessing] = useState<boolean>(false);

  const handleFile = useCallback(
    (files: FileType[]) => {
      const file = files[0].blobFile;
      if (file) {
        setProcessing(true);
        processFile(file)
          .then(onItemProcessed)
          .finally(() => setProcessing(false));
      }
    },
    [onItemProcessed],
  );

  return (
    <Uploader
      accept="audio/*"
      disabled={processing}
      autoUpload={false}
      onChange={handleFile}
      fileList={[]}
      fileListVisible={false}
      className="playlist-menu__add"
    >
      <div>
        {processing ? <Loader className="playlist-menu__add-loader" /> : <Icon icon="upload2" />}
        <span className="playlist-menu__add-text">Nahrát skladbu</span>
      </div>
    </Uploader>
  );
};
