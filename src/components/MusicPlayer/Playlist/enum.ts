export enum PlaylistActions {
  SET_PLAYLIST,
  ADD_RANGE,
  REMOVE_RANGE,
}
