import React, { FC, ReactElement } from 'react';
import { Nav, Icon, Loader } from 'rsuite';
import { PlaylistMenuItemProps } from './types';
import { MusicStatus } from '../MusicProvider/enum';
import { secondsToMinutes } from '../../../helpers';

export const PlaylistMenuItem: FC<PlaylistMenuItemProps> = ({ name, length, onClick, status, active }) => {
  let icon: ReactElement;
  switch (status) {
    case MusicStatus.LOADING: {
      icon = <Loader className="playlist-menu__item-loader" />;
      break;
    }
    case MusicStatus.PLAYING: {
      icon = <Icon icon="pause-circle-o" />;
      break;
    }
    case MusicStatus.PAUSED: {
      icon = <Icon icon="play-circle-o" />;
      break;
    }
    default:
      break;
  }

  return (
    <Nav.Item
      active={active}
      onSelect={() => {
        onClick();
      }}
      className="playlist-menu__item"
      icon={active && icon}
    >
      <span className="playlist-menu__item-name">{name}</span>
      <span className="playlist-menu__item-length">{secondsToMinutes(length)}</span>
    </Nav.Item>
  );
};
