import { MusicStatus } from '../MusicProvider/enum';
import { PlaylistActions } from './enum';

export interface PlaylistItem {
  name: string;
  length: number;
  src: string;
  format: string;
}

export type Playlist = PlaylistItem[];

export interface PlaylistMenuProps {
  initialPlaylist: Playlist;
  show: boolean;
  onHide?: (event?: Event | React.SyntheticEvent) => void;
  selectInitialTrack?: (playlist: Playlist) => PlaylistItem;
  autoplay?: boolean;
}

export interface PlaylistMenuItemProps extends Omit<PlaylistItem, 'src' | 'format'> {
  active: boolean;
  status: MusicStatus;
  onClick: () => void;
}

export interface AddItemProps {
  onItemProcessed: (item: PlaylistItem) => void;
}

export interface PlaylistAction {
  action: PlaylistActions;
  payload?: Playlist;
}
