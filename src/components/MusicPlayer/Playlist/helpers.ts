import { Reducer } from 'react';
import { PlaylistActions } from './enum';
import { Playlist, PlaylistAction, PlaylistItem } from './types';

export const playlistReducer: Reducer<Playlist, PlaylistAction> = (playlist = [], { action, payload }) => {
  switch (action) {
    case PlaylistActions.SET_PLAYLIST:
      return payload;
    case PlaylistActions.ADD_RANGE:
      return [...payload, ...playlist];
    case PlaylistActions.REMOVE_RANGE:
      return playlist.filter(i => payload.findIndex(x => x.src === i.src) === -1);
    default:
      return [];
  }
};

export const processFile = (file: File): Promise<PlaylistItem> =>
  new Promise((resolve, reject) => {
    const audio = document.createElement('audio');
    audio.addEventListener('error', reject);

    const url = URL.createObjectURL(file);

    audio.addEventListener('loadedmetadata', () => {
      const fileNameSplit = file.name.split(/\.(?=[^\\.]+$)/);
      resolve({
        // remove suffix (mp3, etc...)
        name: fileNameSplit[0],
        src: url,
        format: fileNameSplit[1],
        length: audio.duration,
      });
    });
    audio.src = url;
  });
