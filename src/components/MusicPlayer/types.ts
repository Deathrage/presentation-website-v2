export interface MusicPlayerProps {
  onPlaylistClick: (event?: React.SyntheticEvent) => void;
  disablePlaylistButton?: boolean;
}
