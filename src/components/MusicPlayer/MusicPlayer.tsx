import React, { FC, useContext, ReactElement } from 'react';
import { MusicContext } from './MusicProvider/MusicContext';
import { Icon, Loader, Slider, FlexboxGrid, Button } from 'rsuite';
import { MusicStatus } from './MusicProvider/enum';
import { Remaining } from './Remaining/Remaining';
import { MusicPlayerProps } from './types';

export const MusicPlayer: FC<MusicPlayerProps> = ({ children, onPlaylistClick, disablePlaylistButton }) => {
  const { currentMusic, status, pauseMusic, playMusic, setVolume, volume } = useContext(MusicContext);
  const playing = status === MusicStatus.PLAYING;

  let playPauseIcon: ReactElement;
  if (currentMusic && status === MusicStatus.LOADING) playPauseIcon = <Loader className="music-player__loader" />;
  else playPauseIcon = <Icon icon={playing ? 'pause' : 'play'} />;

  let volumeIcon: 'volume-up' | 'volume-down' | 'volume-off';
  if (volume > 0.5) volumeIcon = 'volume-up';
  else if (volume > 0) volumeIcon = 'volume-down';
  else volumeIcon = 'volume-off';

  return (
    <div className="music-player">
      {children}
      <FlexboxGrid className="music-player__panel" align="middle" justify="space-between">
        <FlexboxGrid.Item colspan={3}>
          <Button
            disabled={!currentMusic || status === MusicStatus.LOADING}
            onClick={playing ? pauseMusic : playMusic}
            className="music-player__button"
          >
            {playPauseIcon}
          </Button>
        </FlexboxGrid.Item>
        <FlexboxGrid.Item colspan={6}>
          <Button disabled={disablePlaylistButton} onClick={onPlaylistClick} className="music-player__button">
            <Icon icon="bars" className="music-player__button-icon" />
            Playlist
          </Button>
        </FlexboxGrid.Item>
        <FlexboxGrid.Item colspan={9} className="music-player__volume-slider">
          <Slider
            min={0}
            max={100}
            value={currentMusic ? Number((volume * 100).toFixed(0)) : undefined}
            defaultValue={100}
            step={1}
            onChange={value => setVolume(value / 100)}
            progress
          />
        </FlexboxGrid.Item>
        <FlexboxGrid.Item colspan={2} className="music-player__volume-icon">
          <Icon icon={volumeIcon} />
        </FlexboxGrid.Item>
        <FlexboxGrid.Item colspan={4}>
          <Remaining />
        </FlexboxGrid.Item>
      </FlexboxGrid>
    </div>
  );
};
