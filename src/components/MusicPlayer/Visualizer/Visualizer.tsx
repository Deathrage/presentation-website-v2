import React, { FC, useRef, useContext, useEffect, useState } from 'react';
import { MusicContext } from '../MusicProvider/MusicContext';
import { VisualizerProps, BarInfo, FrameRequest } from './types';
import AnimateHeight from 'react-animate-height';
import { MusicStatus } from '../MusicProvider/enum';

const destroyCanvas = (target: HTMLElement) => {
  const canvas = target.querySelector('canvas');
  canvas && target.removeChild(canvas);
};

const buildCanvas = (target: HTMLElement) => {
  destroyCanvas(target);
  const canvas = document.createElement('canvas');
  target.appendChild(canvas);
  return canvas;
};

const calculateBars = (barWidth: number, barGap: number, canvasWidth: number): BarInfo => {
  const barSpace = barWidth + barGap;

  const ratio = canvasWidth / barSpace;
  const nearestPow = Math.pow(2, Math.round(Math.log(ratio) / Math.log(2)));

  const barCount = nearestPow > 32 ? nearestPow : 64;
  const fftSize = barCount * 2;

  return {
    barCount,
    fftSize,
    barGap,
    barWidth,
  };
};

const clearCanvas = (canvas: HTMLCanvasElement): CanvasRenderingContext2D => {
  const canvasCtx = canvas.getContext('2d');
  canvasCtx.clearRect(0, 0, canvas.width, canvas.height);
  return canvasCtx;
};

const rescaleNumber = (value: number, originalScale: number, newScale: number) => (value / originalScale) * newScale;

const draw = (frameRequest: FrameRequest, canvas: HTMLCanvasElement, barInfo: BarInfo, analyzerNode: AnalyserNode) => {
  frameRequest.id = window.requestAnimationFrame(() => draw(frameRequest, canvas, barInfo, analyzerNode));
  const canvasCtx = clearCanvas(canvas);

  const frequencyBinCount = new Uint8Array(analyzerNode.frequencyBinCount);
  analyzerNode.getByteFrequencyData(frequencyBinCount);

  const barSpace = barInfo.barWidth + barInfo.barGap;
  for (let i = 0; i < barInfo.barCount; i++) {
    const barX = i * barSpace;
    const barHeight = -rescaleNumber(frequencyBinCount[i], 255, canvas.height);
    canvasCtx.fillRect(barX, canvas.height, barInfo.barWidth, barHeight);
  }
};

export const Visualizer: FC<VisualizerProps> = ({ barGap, barWidth }) => {
  const visualizerRef = useRef<HTMLDivElement>();
  const frameRef = useRef<FrameRequest>({ id: null });

  const { context, masterGain, status } = useContext(MusicContext);
  const [show, setShow] = useState<boolean>(status === MusicStatus.PLAYING);

  useEffect(() => setShow(status === MusicStatus.PLAYING), [status]);

  useEffect(() => {
    if (!context || !masterGain) return;

    const canvas = buildCanvas(visualizerRef.current);
    const barInfo = calculateBars(barWidth, barGap, canvas.width);

    const analyser = context.createAnalyser();
    analyser.fftSize = barInfo.fftSize;
    masterGain.connect(analyser);

    const requestRef = frameRef.current;
    draw(requestRef, canvas, barInfo, analyser);

    return () => {
      window.cancelAnimationFrame(requestRef.id);
      clearCanvas(canvas);
      masterGain.disconnect(analyser);
    };
  }, [barGap, barWidth, context, masterGain]);

  return (
    <AnimateHeight duration={500} height={show ? 'auto' : 0}>
      <div ref={visualizerRef} className="visualizer" />
    </AnimateHeight>
  );
};
