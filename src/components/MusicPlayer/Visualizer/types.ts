export interface VisualizerProps {
  barWidth: number;
  barGap: number;
}

export interface BarInfo extends VisualizerProps {
  barCount: number;
  fftSize: number;
}

export interface FrameRequest {
  id: number;
}
