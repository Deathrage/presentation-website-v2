import React, { useState } from 'react';
import { Header, Container, Content, Navbar, Nav } from 'rsuite';
import Sticky from 'react-sticky-el';

import Demos from './Demos';
import { MusicPlayer, PlaylistMenu, Visualizer } from './MusicPlayer';
import { defaultPlaylist } from './defaultPlaylist';

export const App = () => {
  const [showPlaylistMenu, setShowPlaylistMenu] = useState<boolean>(false);

  return (
    <Container>
      <Header>
        <Navbar className="navbar bg-color--dark" appearance="default">
          <Navbar.Header className="navbar__brand">
            <span className="navbar__brand-logo color--light">Lukáš Procházka</span>
          </Navbar.Header>
        </Navbar>
      </Header>
      <section>
        <MusicPlayer
          onPlaylistClick={() => !showPlaylistMenu && setShowPlaylistMenu(true)}
          disablePlaylistButton={showPlaylistMenu}
        >
          <Visualizer barWidth={3} barGap={2} />
          <PlaylistMenu
            initialPlaylist={defaultPlaylist}
            show={showPlaylistMenu}
            onHide={() => showPlaylistMenu && setShowPlaylistMenu(false)}
            autoplay={false}
            selectInitialTrack={list => list[0]}
          />
        </MusicPlayer>
        <Content className="header--offset">
          <Demos />
        </Content>
      </section>
      <Sticky>
        <Header>
          <Navbar appearance="subtle" className="sub-navbar">
            <Navbar.Body>
              <Nav className="sub-nav">
                <Nav.Item className="sub-nav__item">Work</Nav.Item>
                <Nav.Item className="sub-nav__item">Technology</Nav.Item>
                <Nav.Item className="sub-nav__item">Code</Nav.Item>
                <Nav.Item className="sub-nav__item">Contact</Nav.Item>
              </Nav>
            </Navbar.Body>
          </Navbar>
        </Header>
      </Sticky>
      <section style={{ minHeight: '500px' }}>Work</section>
      <section style={{ minHeight: '500px' }}>Code</section>
      <section style={{ minHeight: '500px' }}>Cotant</section>
    </Container>
  );
};
