import React, { FC } from 'react';
import { Button } from 'rsuite';

export const Clickable: FC<{
  onClick: () => void;
  disabled?: boolean;
}> = ({ onClick, children, disabled }) => (
  <Button disabled={disabled} appearance="link" onClick={onClick} className="clickable">
    {children}
  </Button>
);
