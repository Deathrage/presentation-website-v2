import React from 'react';

import placeholder from './misc/placeholder.png';

const EmptyDemo = () => {
  return <img className="demo__canvas" src={placeholder} />;
};

export default EmptyDemo;
