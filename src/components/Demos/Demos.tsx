import React from 'react';
import { demosConfig } from './demos.config';
import { DemoCollection } from './DemoCollection';

export const Demos = () => <DemoCollection config={demosConfig} />;
