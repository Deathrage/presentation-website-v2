import React, { useState, FC, Fragment } from 'react';
import { Drawer, Button } from 'rsuite';
import { DemoDrawerProps } from './types';
import classnames from 'classnames';
import { DemoItem } from './DemoItem';

export const DemoDrawer: FC<DemoDrawerProps> = ({ demos, onItemClick }) => {
  const [showDrawer, setShowDrawer] = useState(false);

  const buttonClass = classnames({
    'demo__drawer-button': true,
    'demo__drawer-button--hidden': showDrawer,
  });

  return (
    <Fragment>
      <div className="demo__drawer-button-container">
        <Button size="lg" onClick={() => setShowDrawer(true)} className={buttonClass}>
          Další demíčka
        </Button>
      </div>
      <Drawer
        className="demo__drawer"
        backdropClassName="demo__drawer-backdrop"
        show={showDrawer}
        placement="top"
        backdrop
        onHide={() => setShowDrawer(false)}
      >
        <Drawer.Body className="demo__drawer-body">
          {demos.map(config => (
            <DemoItem key={config.name} onClick={() => onItemClick(config.id)} {...config} />
          ))}
        </Drawer.Body>
      </Drawer>
    </Fragment>
  );
};
