import React, { useState, FC } from 'react';
import { DemoDrawer } from './DemoDrawer';
import { DemoCollectionProps } from './types';

export const DemoCollection: FC<DemoCollectionProps> = ({ config }) => {
  const [activeDemo, setActiveDemo] = useState<number>(1);
  const demo = config.find(x => x.id === activeDemo);

  return (
    <div className="demo">
      <DemoDrawer demos={config} onItemClick={setActiveDemo} />
      {demo && React.createElement(demo.component, { showPreview: false }, [])}
    </div>
  );
};
