import React, { FC } from 'react';
import { Button, Panel } from 'rsuite';
import { DemoItemProps } from './types';

export const DemoItem: FC<DemoItemProps> = ({ name, component, onClick }) => (
  <Button appearance="ghost" onClick={onClick} className="demo__demo-item-button">
    <Panel header={name} className="demo__demo-item">
      <Panel shaded className="demo__preview demo">
        {React.createElement(component, { showPreview: true }, [])}
      </Panel>
    </Panel>
  </Button>
);
