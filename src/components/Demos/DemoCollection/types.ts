import { FunctionComponent } from 'react';

export interface DemoComponentProps {
  showPreview: boolean;
}

interface DemoConfig {
  id: number;
  name: string;
  component: FunctionComponent<DemoComponentProps>;
}

export type DemoConfigs = DemoConfig[];

export interface DemoItemProps extends DemoConfig {
  onClick: () => void;
}

export interface DemoDrawerProps {
  demos: DemoConfigs;
  onItemClick: (id: number) => void;
}

export interface DemoCollectionProps {
  config: DemoConfigs;
}
