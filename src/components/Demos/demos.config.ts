import { DemoConfigs } from './DemoCollection';
import { EmptyDemo } from './EmptyDemo';
import { Particles } from './Particles';

export const demosConfig: DemoConfigs = [
  {
    id: 0,
    name: 'Empty',
    component: EmptyDemo,
  },
  {
    id: 1,
    name: 'Particles',
    component: Particles,
  },
];
