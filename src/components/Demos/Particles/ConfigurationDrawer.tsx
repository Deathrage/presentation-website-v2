import React, { useState, useRef, useCallback, FC, useContext } from 'react';
import { Sidenav, Icon, IconButton, Nav, ButtonGroup, Button, Uploader, Alert } from 'rsuite';
import { ConfigurationTree } from './ConfigurationTree';
import classNames from 'classnames';
import { useOutboundClick, constants } from '../../../helpers';
import { ConfigurationTreeContext } from './ConfigurationTree/Context';
import { FileType } from 'rsuite/lib/Uploader';

enum Tabs {
  CONFIG,
  PRESETS,
}

const ImportExport: FC<{}> = () => {
  const { setParticlesConfig, particlesConfig } = useContext(ConfigurationTreeContext);

  const download = useCallback(() => {
    const text = JSON.stringify(particlesConfig);
    const element = document.createElement('a');
    element.setAttribute('href', `data:application/json;charset=utf-8,${encodeURIComponent(text)}`);
    element.setAttribute('download', 'particles_config.json');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }, [particlesConfig]);

  const upload = useCallback(
    (file: FileType) => {
      if (!file || !file.blobFile) return;

      const reader = new FileReader();
      reader.onload = event => {
        try {
          const result = JSON.parse(event.target.result as string);
          setParticlesConfig(result);
        } catch {
          Alert.error(
            'File content was not parsed properly. Ensure that the file contains valid JSON.',
            constants.errorAlertTimeout,
          );
        }
      };
      reader.readAsText(file.blobFile);
    },
    [setParticlesConfig],
  );

  return (
    <ButtonGroup className="particles__drawer-footer">
      <Uploader
        fileListVisible={false}
        fileList={[]}
        accept="application/json"
        className="particles__drawer-footer-button"
        onChange={fileList => upload(fileList[0])}
      >
        <div>
          <Icon icon="upload" className="particles__drawer-icon" />
          Import
        </div>
      </Uploader>
      <Button appearance="primary" className="particles__drawer-footer-button" onClick={download}>
        <div>
          <Icon icon="download" className="particles__drawer-icon" />
          Export
        </div>
      </Button>
    </ButtonGroup>
  );
};

const ConfigurationDrawerInner = () => {
  const [show, setShow] = useState<boolean>(true);
  const [activeTab, setActiveTab] = useState<Tabs>(Tabs.CONFIG);

  const handleOutBound = useCallback(() => void setShow(false), []);
  const omitSelect = useCallback((node: Node) => {
    const pickerNode = document.querySelector('.particles__drawer-picker-menu');
    if (!pickerNode) return true;
    return !pickerNode.contains(node);
  }, []);

  const divRef = useRef<HTMLDivElement>();
  useOutboundClick(divRef, handleOutBound, omitSelect);

  return (
    <div className="particles__drawer" ref={divRef}>
      <IconButton
        icon={<Icon icon="cog" className="particles__drawer-button__icon" />}
        size="lg"
        onClick={() => void setShow(!show)}
        appearance="primary"
        className="particles__drawer-button"
      />
      <div
        className={classNames({
          'particles__drawer-side': true,
          'particles__drawer-side--open': show,
        })}
      >
        <Nav className="particles__drawer-nav" activeKey={activeTab} appearance="subtle" onSelect={setActiveTab}>
          <Nav.Item eventKey={Tabs.CONFIG}>Konfigurátor částic</Nav.Item>
          <Nav.Item eventKey={Tabs.PRESETS}>Presety</Nav.Item>
        </Nav>
        <Sidenav appearance="inverse">
          <Sidenav.Body className="particles__drawer-body">
            <ConfigurationTree />
          </Sidenav.Body>
        </Sidenav>
        <ImportExport />
      </div>
    </div>
  );
};

export const ConfigurationDrawer = React.memo(ConfigurationDrawerInner);
