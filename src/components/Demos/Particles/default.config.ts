import { Configuration } from './ConfigurationTree/types';

export const defaultConfig: Configuration = {
  // eslint-disable-next-line @typescript-eslint/camelcase
  'background_color.value': '#ebe7e5',
  'particles.number.value': 60,
  'particles.number.density.enable': true,
  'particles.number.density.value_area': 800,
  'particles.color.value': '#2a3d6b',
  'particles.shape.stroke.width': 0,
  'particles.size.value': 0,
  'particles.opacity.value': 1,
  'particles.move.enable': true,
  'particles.line_linked.enable': true,
  'particles.line_linked.distance': 300,
  'particles.line_linked.opacity': 0.4,
  'particles.line_linked.color': '#2a3d6b',
};
