import React, { FC, useMemo, useState, PureComponent } from 'react';
import ParticlesJS, { IParticlesParams } from 'react-particles-js';
import { ParticlesProps } from './types';
import classNames from 'classnames';
import { ConfigurationDrawer } from './ConfigurationDrawer';
import { schemaConfig } from './schema.config';
import { defaultConfig } from './default.config';
import { ConfigurationTreeContext } from './ConfigurationTree/Context';
import { unflatten } from 'flat';
import { Configuration } from './ConfigurationTree/types';
import { Alert } from 'rsuite';
import { constants } from '../../../helpers';

class ParticlesErrorHandler extends PureComponent {
  constructor(props: any) {
    super(props);
  }

  protected handleParticleError = () => {
    Alert.error(
      'An error occured. This may happen when particles cannot fit within the canvas.',
      constants.errorAlertTimeout,
    );
  };

  componentDidUpdate() {
    window.addEventListener('unhandledrejection', this.handleParticleError);
  }
  componentWillUnmount() {
    window.removeEventListener('unhandledrejection', this.handleParticleError);
  }

  componentDidCatch() {
    this.handleParticleError();
  }

  render() {
    return this.props.children;
  }
}

export const Particles: FC<ParticlesProps> = ({ showPreview = true }) => {
  const [particlesConfig, setParticlesConfig] = useState<Configuration>(defaultConfig);

  // eslint-disable-next-line @typescript-eslint/camelcase
  const {
    background_color: { value: backgroundColor },
    ...restConfig
  } = useMemo<{ background_color: { value: string } } & IParticlesParams>(() => unflatten(particlesConfig), [
    particlesConfig,
  ]);

  const className = classNames({
    // eslint-disable-next-line @typescript-eslint/camelcase
    demo__canvas: true,
    'particles__canvas--preview': showPreview,
  });

  return (
    <ConfigurationTreeContext.Provider
      value={{
        schemaConfig,
        particlesConfig,
        setParticlesConfig,
      }}
    >
      <ParticlesErrorHandler>
        <div className="particles" style={{ backgroundColor }}>
          <ParticlesJS canvasClassName={className} params={restConfig} />
          {!showPreview && <ConfigurationDrawer />}
        </div>
      </ParticlesErrorHandler>
    </ConfigurationTreeContext.Provider>
  );
};
