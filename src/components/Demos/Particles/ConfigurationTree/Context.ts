import { createContext } from 'react';
import { ConfigurationTreeContextType } from './types';

const error = () => {
  throw new Error('Illegal use outside context');
};

export const ConfigurationTreeContext = createContext<ConfigurationTreeContextType>({
  setParticlesConfig: error,
  particlesConfig: null,
  schemaConfig: null,
});
