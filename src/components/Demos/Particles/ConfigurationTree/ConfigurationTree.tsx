import React, { FC, useContext, useMemo, useState, Fragment } from 'react';
// import { parseSchema } from './schemaParser';
import {
  Tree,
  FormGroup,
  ControlLabel,
  CheckboxGroup,
  Checkbox,
  InputNumber,
  Uploader,
  Icon,
  IconButton,
  Slider,
  SelectPicker,
} from 'rsuite';
import {
  TreeNodeInfo,
  ConfigurationTreeContextType,
  ValueTreeNode,
  SchemaValueType,
  Schema,
  SchemaValueComplexType,
  SchemaSelect,
  SchemaRange,
  ValueTreeNodeControl,
} from './types';
import { isValueTreeNode } from './helpers';
import { ConfigurationTreeContext } from './Context';
import { parseSchema } from './schemaParser';
import { SchemaTypes } from './enums';
import ColorPicker from 'rsuite-color-picker';

const ClearControl: FC<{ address: string }> = ({ address }) => {
  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  const value = particlesConfig[address];
  return (
    <IconButton
      disabled={!value}
      className="particles__drawer-clear"
      appearance="subtle"
      icon={<Icon icon="close" />}
      onClick={() => void setParticlesConfig({ ...particlesConfig, [address]: undefined })}
    />
  );
};

const BooleanControl: FC<ValueTreeNodeControl> = ({ address }) => {
  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  return (
    <CheckboxGroup
      className="particles__drawer-checker"
      value={particlesConfig[address] ? [true] : []}
      onChange={value => setParticlesConfig({ ...particlesConfig, [address]: value.length > 0 ? true : false })}
    >
      <Checkbox value={true} />
    </CheckboxGroup>
  );
};

const numberControlFactory = (step: number): FC<ValueTreeNodeControl> => ({ address }) => {
  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  return (
    <InputNumber
      value={(particlesConfig[address] as number) || 0}
      onChange={value => setParticlesConfig({ ...particlesConfig, [address]: value })}
      min={0}
      step={step}
      size="xs"
      className="particles__drawer-input"
    />
  );
};

const ColorPickerControl: FC<ValueTreeNodeControl> = ({ address }) => {
  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  return (
    <div className="particles__drawer-color">
      <ColorPicker
        value={particlesConfig[address] as string}
        defaultValue="rgba(0,0,0,0)"
        onChange={({ rgb: { r, g, b, a } }: any) =>
          setParticlesConfig({ ...particlesConfig, [address]: `rgba(${r}, ${g}, ${b}, ${a})` })
        }
      />
    </div>
  );
};

const UploadControl: FC<ValueTreeNodeControl> = ({ address }) => {
  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  const value = particlesConfig[address];
  return (
    <Fragment>
      <ClearControl address={address} />
      <Uploader
        className="particles__drawer-uploader"
        listType="picture"
        accept="image/*"
        fileList={[]}
        fileListVisible={false}
        onChange={fileType => {
          const file = fileType[0].blobFile;
          if (file) {
            const url = URL.createObjectURL(file);
            setParticlesConfig({ ...particlesConfig, [address]: url });
          }
        }}
      >
        {value ? <img src={value as string} /> : <Icon icon="upload" />}
      </Uploader>
    </Fragment>
  );
};

const RangeControl: FC<ValueTreeNodeControl> = ({ address, type }) => {
  if (!(type instanceof SchemaRange))
    throw new Error(`Type of ${nameof(RangeControl)} cannot be used for type ${type}`);

  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  const value = particlesConfig[address];

  return (
    <Slider
      className="particles__drawer-range"
      graduated
      progress
      value={(value as number) || type.min}
      min={type.min}
      max={type.max}
      step={type.step}
      onChange={newValue => setParticlesConfig({ ...particlesConfig, [address]: newValue })}
    />
  );
};

const SelectControl: FC<ValueTreeNodeControl> = ({ address, type, placeholder }) => {
  if (!(type instanceof SchemaSelect))
    throw new Error(`Type of ${nameof(SelectControl)} cannot be used for type ${type}`);

  const { particlesConfig, setParticlesConfig } = useContext(ConfigurationTreeContext);
  const value = particlesConfig[address];

  return (
    <Fragment>
      <ClearControl address={address} />
      <SelectPicker
        className="particles__drawer-picker"
        menuClassName="particles__drawer-picker-menu"
        placement="bottomEnd"
        data={type.options.map(x => ({ label: x, value: x }))}
        value={value || false}
        searchable={false}
        placeholder={placeholder}
        cleanable={false}
        onChange={newValue => setParticlesConfig({ ...particlesConfig, [address]: newValue })}
      />
    </Fragment>
  );
};

const handleAsComplex = (complexType: SchemaValueComplexType): any => {
  if (complexType instanceof SchemaSelect) {
    return SelectControl;
  } else if (complexType instanceof SchemaRange) {
    return RangeControl;
  }
};

const getValueTreeNodeControl = (type: SchemaValueType): React.FC<ValueTreeNodeControl> => {
  switch (type) {
    case SchemaTypes.BOOLEAN:
      return BooleanControl;
    case SchemaTypes.WHOLE_NUMBER:
      return numberControlFactory(1);
    case SchemaTypes.NUMBER:
      return numberControlFactory(0.01);
    case SchemaTypes.COLOR:
      return ColorPickerControl;
    case SchemaTypes.UPLOAD:
      return UploadControl;
    default:
      return handleAsComplex(type as SchemaValueComplexType);
  }
};

const ValueTreeNodeControl: FC<ValueTreeNode> = ({ type, label, address }) => {
  const Control = getValueTreeNodeControl(type);
  if (!Control) throw new Error(`No control was resolved for the address of ${address}`);

  return (
    <FormGroup className="particles__drawer-form__group">
      <ControlLabel className="particles__drawer-form__label">{label}</ControlLabel>
      {(Control && <Control address={address} type={type} placeholder={label} />) || address}
    </FormGroup>
  );
};

const ConfigurationTreeInner: FC<{ schemaConfig: Schema }> = ({ schemaConfig }) => {
  const [expandedNodes, setExpandedNodes] = useState<string[]>(['particles', 'shape', 'stroke']);

  const structure = useMemo(() => {
    if (!schemaConfig)
      throw new Error(
        `Context property ${nameof<ConfigurationTreeContextType>(x => x.schemaConfig)} of contect ${nameof(
          ConfigurationTreeContext,
        )} must be specified`,
      );

    return parseSchema(schemaConfig);
  }, [schemaConfig]);

  return (
    <Tree
      className="particles__drawer-tree particles__drawer-form"
      data={structure}
      onExpand={ids => void setExpandedNodes(ids)}
      expandItemValues={expandedNodes}
      valueKey="id"
      value={null}
      onSelect={node => {
        if (isValueTreeNode(node)) return;

        const { id } = node;
        if (expandedNodes.includes(id)) setExpandedNodes([...expandedNodes.filter(x => x !== id)]);
        else setExpandedNodes([...expandedNodes, id]);
      }}
      renderTreeNode={(node: TreeNodeInfo) => {
        if (isValueTreeNode(node)) {
          return <ValueTreeNodeControl {...node} />;
        }
        return node.label;
      }}
    />
  );
};

const MemoizedConfigurationTreeInner = React.memo(ConfigurationTreeInner);

export const ConfigurationTree: FC<{}> = () => {
  const { schemaConfig } = useContext(ConfigurationTreeContext);
  return <MemoizedConfigurationTreeInner schemaConfig={schemaConfig} />;
};
