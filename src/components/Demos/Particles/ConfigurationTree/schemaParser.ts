import { Schema, SchemaTreeNode, ValueTreeNode, SchemaValueType } from './types';

const createSchemaNode = (id: string): SchemaTreeNode => ({
  id,
  label: id,
  children: [],
});
const createValueNode = (id: string, address: string, type: SchemaValueType): ValueTreeNode => ({
  id,
  address,
  label: id,
  type,
});

const parseLastAddressBit = (
  parent: SchemaTreeNode,
  lastAddressBit: string,
  fullNodeAddress: string,
  valueType: SchemaValueType,
  isLast: boolean,
): SchemaTreeNode => {
  let currentNode = parent.children.find(x => x.id === lastAddressBit);

  if (isLast && !currentNode) {
    currentNode = createValueNode(lastAddressBit, fullNodeAddress, valueType);
    parent.children.push(currentNode);
  } else if (isLast && currentNode)
    throw new Error(
      `Value node at address ${fullNodeAddress} already exists. Are there two same addresses in the schema?`,
    );
  else if (!currentNode) {
    currentNode = createSchemaNode(lastAddressBit);
    parent.children.push(currentNode);
  }

  return currentNode;
};

const parseSchemaEntry = (rootNode: SchemaTreeNode, entryAddressBits: string[], entryType: SchemaValueType) => {
  let previousNode = rootNode;
  let discoveredAddress = '';
  for (let i = 1; i < entryAddressBits.length; i++) {
    const isLast = i + 1 === entryAddressBits.length;
    const isFirstNonRoot = i === 1;

    const lastAddressBit = entryAddressBits[i];

    discoveredAddress += `${isFirstNonRoot ? `${rootNode.id}.` : '.'}${lastAddressBit}`;

    previousNode = parseLastAddressBit(previousNode, lastAddressBit, discoveredAddress, entryType, isLast);
  }

  return rootNode;
};

export const parseSchema = (schema: Schema): SchemaTreeNode[] => {
  const roots: SchemaTreeNode[] = [];

  for (const entry of Object.entries(schema)) {
    const [address, type] = entry;
    const addressBits = address.split('.');

    const rootId = addressBits[0];
    const existingRootNode = roots.find(x => x.id === rootId);

    const root = parseSchemaEntry(existingRootNode || createSchemaNode(rootId), addressBits, type);

    !existingRootNode && roots.push(root);
  }

  return roots;
};
