import { Schema, Configuration, TreeNodeInfo, ValueTreeNode } from './types';

export const validateSchemaIntegrity = (schema: Schema, configuration: Configuration) =>
  ((schema as any) + configuration) as any;

export const isValueTreeNode = (node: TreeNodeInfo): node is ValueTreeNode =>
  Reflect.has(
    node,
    nameof<ValueTreeNode>(x => x.address),
  ) &&
  Reflect.has(
    node,
    nameof<ValueTreeNode>(x => x.type),
  );
