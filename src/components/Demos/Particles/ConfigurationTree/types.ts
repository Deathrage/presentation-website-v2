import { SchemaTypes } from './enums';

export type SchemaValueComplexType = SchemaSelect<string> | SchemaRange;
export type SchemaValueType = SchemaTypes | SchemaValueComplexType;
export type ConfigurationValueType = string | boolean | number | string[];

export interface TreeNodeInfo {
  id: string;
  label: string;
}

export interface SchemaTreeNode extends TreeNodeInfo {
  children?: SchemaTreeNode[];
}

export interface ValueTreeNode extends TreeNodeInfo {
  type: SchemaValueType;
  address: string;
}

export class SchemaSelect<T> {
  constructor(public type: SchemaTypes.SELECT | SchemaTypes.SELECT_MULTIPLE, public options: T[]) {}
}
export class SchemaRange {
  public type = SchemaTypes.NUMBER;
  constructor(public min: number, public max: number, public step: number) {}
}

export interface Schema {
  [key: string]: SchemaValueType;
}

export type Configuration = {
  [P in keyof Schema]?: ConfigurationValueType;
};

export interface ConfigurationTreeContextType {
  setParticlesConfig: (configuration: Configuration) => void;
  particlesConfig: Configuration;
  schemaConfig: Schema;
}

export interface ValueTreeNodeControl {
  address: string;
  type: SchemaValueType;
  placeholder: string;
}
