import { Playlist } from './MusicPlayer/Playlist/types';

import BlueMoonRises from '../../static/music/NGHFB-BlueMoonRising.mp3';

export const defaultPlaylist: Playlist = [
  {
    name: 'NGHFB - Blue Moon Rises',
    format: 'mp3',
    length: 225,
    src: BlueMoonRises,
  },
  {
    name: 'Hnn',
    length: 225,
    src: 'bar',
    format: 'mp3',
  },
];
