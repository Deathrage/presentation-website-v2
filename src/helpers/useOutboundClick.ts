import { useEffect, useCallback, RefObject } from 'react';

export const useOutboundClick = (
  ref: RefObject<HTMLElement>,
  handler: (event?: MouseEvent) => void,
  additionalComparer?: (target: Node) => boolean,
) => {
  const handleClickOutside = useCallback(
    (event: MouseEvent) => {
      const additionalResult = additionalComparer ? additionalComparer(event.target as Node) : true;
      if (ref.current && !ref.current.contains(event.target as Node) && additionalResult) handler && handler(event);
    },
    [additionalComparer, ref, handler],
  );

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => void document.removeEventListener('mousedown', handleClickOutside);
  }, [handleClickOutside]);
};
