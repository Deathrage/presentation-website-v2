export const secondsToMinutes = (source: number) => {
  const minutes = Math.floor(source / 60);
  const seconds = Number((source % 60).toFixed(0));
  return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
};

export const millisecondsToMinutes = (source: number) => {
  const minutes = Math.floor(source / 60000);
  const seconds = Number(((source % 60000) / 1000).toFixed(0));
  return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
};
