import { secondsToMinutes, millisecondsToMinutes } from './Time';

describe('Test time helpers', () => {
  it('Should format seconds to minute string', () => {
    const source = 225;
    expect(secondsToMinutes(source)).toEqual('3:45');
  });
  it('Should format milliseconds to minute string', () => {
    const source = 225000;
    expect(millisecondsToMinutes(source)).toEqual('3:45');
  });
});
