/// <reference types="react-dom/experimental" />
/// <reference types="react/experimental" />

import React from 'react';
import ReactDOM from 'react-dom';
import 'rsuite-color-picker/lib/styles.less';

import { App, MusicProvider } from './components';

const Bootstrap = () => (
  <MusicProvider>
    <App />
  </MusicProvider>
);

export const bootstrap = () => {
  ReactDOM.createRoot(document.getElementById('app') as HTMLElement).render(<Bootstrap />);
};
